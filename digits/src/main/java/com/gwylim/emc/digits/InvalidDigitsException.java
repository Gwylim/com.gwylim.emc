package com.gwylim.emc.digits;

/**
 * This exception is thrown in the event of a {@link DigitExtrationStrategy} finding a character that is cannot convert to a digit.
 */
@SuppressWarnings("serial")
public class InvalidDigitsException extends Exception {

  private final String _input;

  /**
   * Create an InvalidDigitsException that was caused by the given input.
   */
  public InvalidDigitsException(final String input) {
    _input = input;
  }

  /**
   * Get the input that caused the issue.
   */
  public String getInput() {
    return _input;
  }
}
