package com.gwylim.emc.digits.reduce;

import java.util.List;

import com.gwylim.emc.digits.DigitExtrationStrategy;
import com.gwylim.emc.digits.DigitReducer;
import com.gwylim.emc.digits.InvalidDigitsException;

/**
 * Implements an iterative reduction of the digits in the supplied String.
 * The String value is used at each stage to determine if a single digit has been reached.
 * @author Gwylim
 */
public class IterativeDigitReducer implements DigitReducer {
  private final DigitExtrationStrategy _digitExtrationStrategy;

  /**
   * Create a IterativeDigitReducer with the supplied {@link DigitExtrationStrategy}.
   * @param digitExtrationStrategy The {@link DigitExtrationStrategy} to use.
   */
  public IterativeDigitReducer(final DigitExtrationStrategy digitExtrationStrategy) {
    _digitExtrationStrategy = digitExtrationStrategy;
  }

  /**
   * @see DigitReducer#reduce(String)
   */
  @Override
  public int reduce(final String input) throws InvalidDigitsException {
    List<Integer> digits = _digitExtrationStrategy.aquire(input);

    while (digits.size() != 1) {
      final int total = DigitReducer.total(digits);
      digits = _digitExtrationStrategy.aquire(Integer.toString(total));
    }

    return digits.get(0);
  }
}
