package com.gwylim.emc.digits.reduce;

import java.util.List;

import com.gwylim.emc.digits.DigitExtrationStrategy;
import com.gwylim.emc.digits.DigitReducer;
import com.gwylim.emc.digits.InvalidDigitsException;

/**
 * Implements recursive reduction of the digits in the supplied String.
 * The String value is used at each stage to determine if a single digit has been reached.
 * @author Gwylim
 */
public class RecursiveDigitReducer implements DigitReducer {
  private final DigitExtrationStrategy _digitExtrationStrategy;

  /**
   * Create a new {@link RecursiveDigitReducer} that uses the supplied {@link DigitExtrationStrategy}.
   * @param digitExtrationStrategy The {@link DigitExtrationStrategy} to use.
   */
  public RecursiveDigitReducer(final DigitExtrationStrategy digitExtrationStrategy) {
    _digitExtrationStrategy = digitExtrationStrategy;
  }

  /**
   * Performs the recursive collapse of digits.
   * 1. Get the current digits.
   * 2. If there is only one return it.
   * 3. Otherwise calculate the total and recurse on it.
   *
   * @see DigitReducer#reduce(String)
   */
  @Override
  public int reduce(final String input) throws InvalidDigitsException {
    // Parse the digits and use them in termination condition instead of input.length() so the input is always validated.
    final List<Integer> digits = _digitExtrationStrategy.aquire(input);

    // Termination condition: there is only one digit left.
    if (digits.size() == 1) { return digits.get(0); }

    // If there are more than one digits left collapse and recurse on the total of current digits.
    final int total = DigitReducer.total(digits);
    return reduce(Integer.toString(total));
  }
}
