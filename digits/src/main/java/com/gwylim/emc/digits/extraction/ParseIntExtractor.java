package com.gwylim.emc.digits.extraction;

import java.util.ArrayList;
import java.util.List;

import com.gwylim.emc.digits.DigitExtrationStrategy;
import com.gwylim.emc.digits.InvalidDigitsException;

/**
 * Implementation of {@link DigitExtrationStrategy} that uses {@link Integer#parseInt(String)} to attempt to parse each character in the string.
 * @author Gwylim
 */
public class ParseIntExtractor  implements DigitExtrationStrategy {

  /**
   * @see DigitExtrationStrategy#aquire(String)
   */
  @Override
  public List<Integer> aquire(final String input) throws InvalidDigitsException {
    if (input.isEmpty()) { throw new InvalidDigitsException(input); }

    final List<Integer> result = new ArrayList<Integer>(input.length());

    for (int i = 0; i < input.length(); i++) {
      final char currentCharacter = input.charAt(i);
      try {
        result.add(Integer.parseInt(Character.toString(currentCharacter)));
      }
      catch (final NumberFormatException e) {
        throw new InvalidDigitsException(Character.toString(currentCharacter));
      }
    }

    return result;
  }

}
