package com.gwylim.emc.digits.reduce;


/**
 * Perform a recursive digit reduction using integer operations.
 * @author Gwylim
 */
public class IntegerIterativeReduce extends IntegerReduceBase {

  @Override
  protected int reduce(final long input) {

    long remainder = input;
    long reduction = input;

    // CHECKSTYLE:OFF: MagicNumber
    while (reduction > 9) {          // While there is more than one digit remaining.
      remainder = reduction;         // The current digits.

      // Perform single reduction step:
      reduction = 0;                 // The result of reduction.
      while (remainder > 0) {        // While there are still digits left.
        reduction += remainder % 10; // Add the the lease significant digit to the result.
        remainder /= 10;             // Remove the lease significant digit.
      }
    }
    // CHECKSTYLE:ON: MagicNumber

    return (int) reduction;
  }
}
