package com.gwylim.emc.digits;

import java.util.List;

/**
 *  Strategies for extracting the numbers from a string of text.
 *  The text must consist of only digit characters or an exception will be thrown.
 *
 *  NOTE:
 *  This is over-engineered, I would probably just use an in-lined version of {@link com.gwylim.emc.digits.reduce.IntegerRecursiveReduce} as it is clearer.
 */
public interface DigitExtrationStrategy {

  /**
   * Extract the digits from a the given input.
   * The input must contain only the characters 0-9 or an {@link InvalidDigitsException} will be throw.
   * @param digits  This {@link String} to extract digits from.
   * @return A {@link List} of the digits as Integer Objects. This will be the empty List when the string is empty.
   * @throws InvalidDigitsException If the string could not be converted into digits.
   */
  List<Integer> aquire(String digits) throws InvalidDigitsException;
}
