package com.gwylim.emc.digits.extraction;

import java.util.ArrayList;
import java.util.List;

import com.gwylim.emc.digits.DigitExtrationStrategy;
import com.gwylim.emc.digits.InvalidDigitsException;

/**
 * Implementation of DigitExtrationStrategy that uses the character code to determine digit validity/value.
 *
 * NOTE:
 * I would expect higher performance from this over {@link ParseIntExtractor} but there is probably not much in it.
 * TODO: Performance test.
 *
 * @author Gwylim
 */
public class CharValueExtractor implements DigitExtrationStrategy {

  /**
   * @see DigitExtrationStrategy#aquire(String)
   */
  @Override
  public List<Integer> aquire(final String input) throws InvalidDigitsException {
    if (input.isEmpty()) { throw new InvalidDigitsException(input); }

    final List<Integer> result = new ArrayList<Integer>(input.length());

    for (int i = 0; i < input.length(); i++) {
      final char c = input.charAt(i);
      if (c >= '0' && c <= '9') {
        result.add(c - '0');
      }
      else {
        throw new InvalidDigitsException(Character.toString(c));
      }
    }

    return result;
  }

}
