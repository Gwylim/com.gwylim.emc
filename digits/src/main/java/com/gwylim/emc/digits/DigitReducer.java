package com.gwylim.emc.digits;

import java.util.List;
import java.util.Optional;

/**
 * Object to encapsulate digit collapse algorithms.
 *
 * Requirement:
 * Take a number and reduce it to a single digit by adding the individual digits.
 * For example, 359 would be 3 + 5 + 9 = 17 = 1 + 7 = 8.
 *
 * @author Gwylim
 */
public interface DigitReducer {

  /**
   * Perform the digit reduction on the supplied string.
   * @param input  The input digits to perform a reduction on. This must contain only the characters 0-9.
   * @return  The value of the remaining digit after reduction.
   * @throws InvalidDigitsException If the string contained characters that could not be converted to digits.
   */
  int reduce(String input) throws InvalidDigitsException;


  /**
   * Utility method to sum the values in a List of Integers.
   * @param digits The List of Integers to sum.
   * @return  The sum of the Integers. 0 if the input list is empty.
   */
  static int total(final List<Integer> digits) {
    final Optional<Integer> total = digits.parallelStream().reduce((a, b) -> a + b);
    return total.orElse(0); // Default to zero if the list was empty.
  }

}