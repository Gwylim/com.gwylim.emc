package com.gwylim.emc.digits.reduce;

import com.gwylim.emc.digits.DigitReducer;
import com.gwylim.emc.digits.InvalidDigitsException;

/**
 * Base class for performing digit reduction using integer operations.
 * NB: A limitation of these reducers is that they are limited to the size of a Long.
 *
 * @author Gwylim
 */
public abstract class IntegerReduceBase implements DigitReducer {

  @Override
  public final int reduce(final String input) throws InvalidDigitsException {
    try {
      final long value = Long.parseLong(input);
      return reduce(value);
    }
    catch (final NumberFormatException e) {
      throw new InvalidDigitsException(input);
    }
  }

  /**
   * Method to provide the implementation of the integer reduction.
   * @param integer The integer to be reduced.
   * @return  The reduction of th supplied integer.
   */
  protected abstract int reduce(long integer);
}
