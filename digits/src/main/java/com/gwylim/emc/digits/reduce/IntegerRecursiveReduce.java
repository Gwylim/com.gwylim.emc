package com.gwylim.emc.digits.reduce;


/**
 * Perform an iterative digit reduction using integer operations.
 * @author Gwylim
 */
public class IntegerRecursiveReduce extends IntegerReduceBase {

  /**
   * @see IntegerReduceBase#reduce(long)
   */
  @Override
  protected int reduce(final long input) {
    return (int) reduceImpl(input);
  }

  /**
   * Perform the recursive reduce with long return type to avoid integer size limitation.
   * @param input The number to be reduced.
   * @return The reduced value.
   */
  protected long reduceImpl(final long input) {
    long remainder = input;

    // CHECKSTYLE:OFF: MagicNumber
    // Perform single reduction step:
    long reduction = 0;
    while (remainder > 0) {         // While there are still digits:
      reduction += remainder % 10;  // Add the least significant to the result.
      remainder /= 10;              // Then remove it.
    }

    if (reduction <= 9) {
      return reduction;
    }
    else {
      return reduce(reduction);
    }
    // CHECKSTYLE:ON: MagicNumber
  }
}
