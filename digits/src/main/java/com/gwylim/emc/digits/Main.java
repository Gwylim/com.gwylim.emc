package com.gwylim.emc.digits;

import com.gwylim.emc.digits.reduce.IntegerIterativeReduce;

/**
 * Run digit reduction.
 * @author Gwylim
 */
public class Main {

  /**
   * Runs digit reduction on the one and only argument to the program.
   * @param args A single entry array containing the digit string to be reduced.
   */
  public static void main(final String[] args) {
    if (args.length != 1) {
      System.out.println("This program takes exactly one argument; the digits to be reduced.");
      return;
    }
    final String input = args[0];



    final DigitReducer reducer = new IntegerIterativeReduce();

    try {
      final int reduction = reducer.reduce(input);

      System.out.println("The input '" + input + "' was reduced to '" + reduction + "'.");
    }
    catch (final InvalidDigitsException e) {
      System.err.println("Could not parse the digits in the input '" + input + "' all characters must be numbers.");
    }
  }

}
