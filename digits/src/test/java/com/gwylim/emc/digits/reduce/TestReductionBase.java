package com.gwylim.emc.digits.reduce;

import org.junit.Assert;
import org.junit.Test;

import com.gwylim.emc.digits.DigitReducer;
import com.gwylim.emc.digits.InvalidDigitsException;

/**
 * Base class for testing {@link DigitReducer}s.
 * @author Gwylim
 */
public abstract class TestReductionBase {

  protected abstract DigitReducer getDigitReducer();

  @Test(expected = InvalidDigitsException.class)
  public void testEmpty() throws Exception {
    Assert.assertEquals(0, getDigitReducer().reduce(""));
  }

  @Test
  public void testSingleDigit() throws Exception {
    Assert.assertEquals(0, getDigitReducer().reduce("0"));
    Assert.assertEquals(1, getDigitReducer().reduce("1"));
    Assert.assertEquals(2, getDigitReducer().reduce("2"));
    Assert.assertEquals(3, getDigitReducer().reduce("3"));
    Assert.assertEquals(4, getDigitReducer().reduce("4"));
    Assert.assertEquals(5, getDigitReducer().reduce("5"));
    Assert.assertEquals(6, getDigitReducer().reduce("6"));
    Assert.assertEquals(7, getDigitReducer().reduce("7"));
    Assert.assertEquals(8, getDigitReducer().reduce("8"));
    Assert.assertEquals(9, getDigitReducer().reduce("9"));
  }

  @Test
  public void testDoubleDigitToSingle() throws Exception {
    Assert.assertEquals(9, getDigitReducer().reduce("54"));
    Assert.assertEquals(2, getDigitReducer().reduce("11"));
  }

  @Test
  public void testDoubleToDoubleToSingle() throws Exception {
    Assert.assertEquals(9, getDigitReducer().reduce("99"));
    Assert.assertEquals(7, getDigitReducer().reduce("88"));
  }

  @Test(expected = InvalidDigitsException.class)
  public void testBadCharacter() throws Exception {
    getDigitReducer().reduce("X");
  }

  @Test
  public void testQuiteLarge() throws Exception {
    getDigitReducer().reduce(Long.toString(Long.MAX_VALUE));
  }
}
