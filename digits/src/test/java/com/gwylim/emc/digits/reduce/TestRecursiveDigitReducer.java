package com.gwylim.emc.digits.reduce;

import org.junit.Test;

import com.gwylim.emc.digits.DigitReducer;
import com.gwylim.emc.digits.extraction.CharValueExtractor;

public class TestRecursiveDigitReducer extends TestReductionBase {

  @Override
  protected DigitReducer getDigitReducer() {
    return new RecursiveDigitReducer(new CharValueExtractor());
  }

  @Test
  public void testVeryLarge() throws Exception {
    getDigitReducer().reduce("873492650132674501967349526340917537849625093470851304978562083746509283475");
  }
}
