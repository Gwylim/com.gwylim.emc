package com.gwylim.emc.digits.reduce;

import com.gwylim.emc.digits.DigitReducer;

public class TestIntegerRecursiveReducer extends TestReductionBase {

  @Override
  protected DigitReducer getDigitReducer() {
    return new IntegerRecursiveReduce();
  }

}
