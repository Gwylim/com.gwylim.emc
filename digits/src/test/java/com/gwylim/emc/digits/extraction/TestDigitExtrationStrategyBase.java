package com.gwylim.emc.digits.extraction;

import java.util.Arrays;
import java.util.Collections;

import junitx.framework.ListAssert;

import org.junit.Test;

import com.gwylim.emc.digits.DigitExtrationStrategy;
import com.gwylim.emc.digits.InvalidDigitsException;

/**
 * Base class for testing {@link DigitExtrationStrategy}s.
 * This should be extended to supply the {@link DigitExtrationStrategy} under test.
 * It will run a suit of tests against the supplied {@link DigitExtrationStrategy}.
 *
 * @author Gwylim
 */
public abstract class TestDigitExtrationStrategyBase {

  protected abstract DigitExtrationStrategy getStrategyUnderTest();

  /**
   * Test the response for an empty String.
   * Expecting empty list.
   */
  @Test(expected = InvalidDigitsException.class)
  public void testEmpty() throws Exception {
    ListAssert.assertEquals(Collections.emptyList(), getStrategyUnderTest().aquire(""));
  }

  /**
   * Test all single characters we accept. ie: The digits 0-9.
   */
  @Test
  public void testSingle() throws Exception {
    ListAssert.assertEquals(Collections.singletonList(0), getStrategyUnderTest().aquire("0"));
    ListAssert.assertEquals(Collections.singletonList(1), getStrategyUnderTest().aquire("1"));
    ListAssert.assertEquals(Collections.singletonList(2), getStrategyUnderTest().aquire("2"));
    ListAssert.assertEquals(Collections.singletonList(3), getStrategyUnderTest().aquire("3"));
    ListAssert.assertEquals(Collections.singletonList(4), getStrategyUnderTest().aquire("4"));
    ListAssert.assertEquals(Collections.singletonList(5), getStrategyUnderTest().aquire("5"));
    ListAssert.assertEquals(Collections.singletonList(6), getStrategyUnderTest().aquire("6"));
    ListAssert.assertEquals(Collections.singletonList(7), getStrategyUnderTest().aquire("7"));
    ListAssert.assertEquals(Collections.singletonList(8), getStrategyUnderTest().aquire("8"));
    ListAssert.assertEquals(Collections.singletonList(9), getStrategyUnderTest().aquire("9"));
  }

  /**
   * Test multiple acceptable characters.
   */
  @Test
  public void testMultiple() throws Exception {
    ListAssert.assertEquals(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9), getStrategyUnderTest().aquire("0123456789"));
  }

  /**
   * Test that characters that are not 0-9 cause an exception.
   */
  @Test(expected = InvalidDigitsException.class) public void testSingleBadCaharacterAlpha() throws Exception { getStrategyUnderTest().aquire("X"); }
  @Test(expected = InvalidDigitsException.class) public void testSingleBadCaharacterHash() throws Exception { getStrategyUnderTest().aquire("#"); }
  @Test(expected = InvalidDigitsException.class) public void testSingleBadCaharacterDecimalPoint() throws Exception { getStrategyUnderTest().aquire("."); }
  @Test(expected = InvalidDigitsException.class) public void testSingleBadCaharacterExponent() throws Exception { getStrategyUnderTest().aquire("E"); }

  /** Test that a collection of bad characters still cause an exception. */
  @Test(expected = InvalidDigitsException.class) public void testMultipleBadCaharactersAlpha() throws Exception { getStrategyUnderTest().aquire("XYZ"); }
  @Test(expected = InvalidDigitsException.class) public void testMultipleBadCaharactersSymbol() throws Exception { getStrategyUnderTest().aquire("#.%"); }

  /** Test that a mix of digits and bad characters cause an exception.  */
  @Test(expected = InvalidDigitsException.class) public void testBadCharDigitMixStart() throws Exception { getStrategyUnderTest().aquire("123X567"); }
  @Test(expected = InvalidDigitsException.class) public void testBadCharDigitMixMiddle() throws Exception { getStrategyUnderTest().aquire("X123567"); }
  @Test(expected = InvalidDigitsException.class) public void testBadCharDigitMixEnd() throws Exception { getStrategyUnderTest().aquire("123567X"); }

  /** Test that a character that can be represented as a single byte but is not ASCII causes the expected exception. */
  @Test(expected = InvalidDigitsException.class) public void testNonAsciiCharacter() throws Exception { getStrategyUnderTest().aquire("¢"); }
  /** Test that a character that uses more than one byte in UTF-8 causes the expected exception. */
  @Test(expected = InvalidDigitsException.class) public void testMultiByteCharacter() throws Exception { getStrategyUnderTest().aquire("丏"); }
}
