package com.gwylim.emc.digits.reduce;

import com.gwylim.emc.digits.DigitReducer;

public class TestIntegerItterativeReducer extends TestReductionBase {

  @Override
  protected DigitReducer getDigitReducer() {
    return new IntegerIterativeReduce();
  }

}
