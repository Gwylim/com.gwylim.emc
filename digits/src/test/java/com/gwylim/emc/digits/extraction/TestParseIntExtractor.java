package com.gwylim.emc.digits.extraction;

import com.gwylim.emc.digits.DigitExtrationStrategy;


public class TestParseIntExtractor extends TestDigitExtrationStrategyBase {

  @Override
  protected DigitExtrationStrategy getStrategyUnderTest() {
    return new ParseIntExtractor();
  }

}
