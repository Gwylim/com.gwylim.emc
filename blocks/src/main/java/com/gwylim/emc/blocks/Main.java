package com.gwylim.emc.blocks;

import java.util.Set;

/**
 * Main class that runs the grid given in the task description.
 * @author Gwylim
 */
public class Main {

  /**
   * Find the biggest block in the grid provided by the task description.
   * @param args NONE
   * @throws MalformedBlockException If the grid is not parsable.
   */
  public static void main(final String[] args) throws MalformedBlockException {
    final Grid grid = new Grid(" A C A F F F F ",
                               " A C C C C E E ",
                               " A D D A C E D ",
                               " B B A B A A D ",
                               " B B B B B D D ");

    System.out.println(grid);

    final Set<Cell> largestBlock = grid.largestBlock();

    System.out.println(largestBlock);
    System.out.println(largestBlock.size());
  }
}
