package com.gwylim.emc.blocks;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * A representation of a 2 dimensional grid of characters.
 * NB: The origin of the grid is upper left.
 *
 * @author Gwylim
 */
public class Grid {

  private final String[][] _grid;

  /**
   * Create a new grid by parsing an grid of characters.
   * The lines must be the same length.
   * @param block The grid of characters to parse.
   * @throws MalformedBlockException  If the input is not the correct shape.
   */
  public Grid(final String block) throws MalformedBlockException {
    this(block.split("\n"));
  }

  /**
   * Create a new grid by parsing an number of lines of characters.
   * The lines must be the same length.
   * @param rows The rows of characters to parse.
   * @throws MalformedBlockException  If the input is not the correct shape.
   */
  public Grid(final String... rows) throws MalformedBlockException {
    final int rowSize = rows[0].replaceAll("\\s", "").length();

    final String[][] blockGrid = new String[rowSize][rows.length];

    for (int y = 0; y < rows.length; y++) {
      final String row = rows[y].replaceAll("\\s", "");
      if (row.length() != rowSize) {
        throw new MalformedBlockException();
      }

      for (int x = 0; x < row.length(); x++) {
        final char character = row.charAt(x);
        blockGrid[x][y] = Character.toString(character);
      }
    }

    _grid = blockGrid;
  }


  /**
   * Calculate and return the largest block of characters in the grid.
   * @return  The largest block as a set of cells contained in that block.
   */
  public Set<Cell> largestBlock() {
    final Set<Set<Cell>> blocks = blocks();

    Set<Cell> currentLargest = Collections.emptySet();
    for (final Set<Cell> block : blocks) {
      if (block.size() > currentLargest.size()) {
        currentLargest = block;
      }
    }

    return currentLargest;
  }

  /**
   * Calculate and return all the blocks in the grid.
   * @return  A set of sets of {@link Cell}s, the inner sets being blocks.
   */
  public Set<Set<Cell>> blocks() {
    final Set<Cell> explored = new LinkedHashSet<Cell>();

    final Set<Set<Cell>> blocks = new LinkedHashSet<Set<Cell>>();

    for (int y = 0; y < height(); y++) {
      for (int x = 0; x < width(); x++) {
          final Cell currentCell = getCell(x, y);
          if (explored.contains(currentCell)) {
            continue;
          }

          final Set<Cell> currentBlock = currentCell.getBlock();
          blocks.add(currentBlock);
          explored.addAll(currentBlock); // We don't need to explore cells contained in current block.
      }
    }
    return blocks;
  }


  /**
   * Get the value located in the cell with the given coordinates.
   * NB: origin is 0,0.
   * @throws {@link ArrayIndexOutOfBoundsException} if either coordinate is too large or &lt; 0.
   */
  public String value(final int x, final int y) {
    return _grid[x][y];
  }

  /**
   * Get the {@link Cell} object referencing the given coordinates.
   * NB: origin is 0,0.
   * @throws {@link ArrayIndexOutOfBoundsException} if either coordinate is too large or &lt; 0.
   */
  public Cell getCell(final int x, final int y) {
    final Cell gridCell = new Cell(x, y, this);
    gridCell.getValue(); // Force validation of the coordinates.
    return gridCell;
  }

  /** The width of the grid. */
  public int width() { return _grid.length; }
  /** The height of the grid. */
  public int height() { return _grid[0].length; }


  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder();

    for (int y = 0; y < height(); y++) {
      for (int x = 0; x < width(); x++) {
        sb.append(value(x, y));
        sb.append(" ");
      }
      sb.append("\n");
    }

    return sb.toString();
  }
}
