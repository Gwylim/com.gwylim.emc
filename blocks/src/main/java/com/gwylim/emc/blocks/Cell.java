package com.gwylim.emc.blocks;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Represents a cell within a grid.
 *
 * @author Gwylim
 */
public class Cell {

  private final int _x;
  private final int _y;
  private final Grid _grid;

  /**
   * Create a cell object that gets its value from the provided {@link Grid}.
   * NB: Origin is upper left of the grid.
   *
   * @param x The horizontal location of the {@link Cell} within the {@link Grid}.
   * @param y The vertical location of the {@link Cell} within the {@link Grid}
   * @param grid The {@link Grid} that the cell is referencing.
   */
  public Cell(final int x, final int y, final Grid grid) {
    _x = x;
    _y = y;
    _grid = grid;
  }

  /** Get the x coordinate of this cell. */
  public int getX() { return _x; }
  /** Get the y coordinate of this cell. */
  public int getY() { return _y; }
  /** Get the value of this cell in the grid. */
  public String getValue() { return _grid.value(_x, _y); }

  /**
   * Get the neighbouring cells to this one.
   * Two cells aren’t neighbouring if they are touching each other diagonally.
   * NO wrapping edge to edge.
   */
  public Set<Cell> getNeighbours() {
    final Set<Cell> neighbours = new LinkedHashSet<Cell>();

    if (_x + 1 < _grid.width()) { neighbours.add(_grid.getCell(_x + 1, _y)); }
    if (_x - 1 >= 0) { neighbours.add(_grid.getCell(_x - 1, _y)); }
    if (_y + 1 < _grid.height()) { neighbours.add(_grid.getCell(_x, _y + 1)); }
    if (_y - 1 >= 0) { neighbours.add(_grid.getCell(_x, _y - 1)); }

    return neighbours;
  }

  /**
   * Find all the cells connected to this one by value and neighbour:
   * Block is connected if its neighbouring cell, horizontally or vertically, has the same value.
   *
   * @return The {@link Set} of all {@link Cell}s in the block including this cell.
   */
  public Set<Cell> getBlock() {
    final Set<Cell> block = new LinkedHashSet<Cell>(Collections.singleton(this));

    Set<Cell> fronteer = new LinkedHashSet<Cell>(Collections.singleton(this));

    while (fronteer.size() != 0) {
      final Set<Cell> newFronteer = new LinkedHashSet<Cell>();

      for (final Cell fronteerCell : fronteer) {
        final Set<Cell> neighbours = fronteerCell.getNeighbours();
        for (final Cell fronteerCellNeighbour : neighbours) {
          if (fronteerCellNeighbour.getValue().equals(getValue())) {
            if (block.add(fronteerCellNeighbour)) { // If we have not already added this cell to the block.
              newFronteer.add(fronteerCellNeighbour);
            }
          }
        }
      }

      fronteer = newFronteer;
    }

    return block;
  }



  @Override
  public String toString() {
    return "Cell(" + _x + ", " + _y + ") = " + getValue();
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + (_grid == null ? 0 : _grid.hashCode());
    result = prime * result + _x;
    result = prime * result + _y;
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) { return true; }
    if (obj == null) { return false; }
    if (getClass() != obj.getClass()) { return false; }
    final Cell other = (Cell) obj;
    if (_grid == null) {
      if (other._grid != null) { return false; }
    }
    else if (!_grid.equals(other._grid)) { return false; }
    if (_x != other._x) { return false; }
    if (_y != other._y) { return false; }
    return true;
  }
}
