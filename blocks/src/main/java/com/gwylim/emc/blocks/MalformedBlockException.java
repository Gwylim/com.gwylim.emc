package com.gwylim.emc.blocks;

/**
 * Thrown when there as a failure to parse a block from a {@link String}.
 * @author Gwylim
 */
@SuppressWarnings("serial")
public class MalformedBlockException extends Exception {

}
