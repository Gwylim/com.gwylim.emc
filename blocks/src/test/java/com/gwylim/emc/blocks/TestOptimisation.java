package com.gwylim.emc.blocks;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import junit.framework.Assert;

import org.junit.Test;

/**
 * Test the number of calls to GetCell is constant irrespective of grouping and grid size.
 * Indicating O(n) where n = number of cells.
 * Assuming low enough loading of the HashSets used for already visited checks so look up is ~O(1).
 *
 * @author Gwylim
 */
public class TestOptimisation {

  /**
   * Extension of {@link Grid} who's only function is to record the number of calls to get each cell.
   * @author Gwylim
   */
  private static class GridShim extends Grid {
    private final Map<Cell, AtomicInteger> _cellRequests = new LinkedHashMap<>();

    public GridShim(final String... rows) throws MalformedBlockException {
      super(rows);
    }

    @Override
    public Cell getCell(final int x, final int y) {
      final Cell cell = super.getCell(x, y);

      if (_cellRequests.containsKey(cell)) {
        _cellRequests.get(cell).incrementAndGet();
      }
      else {
        _cellRequests.put(cell, new AtomicInteger(1));
      }

      return cell;
    }

    public Map<Cell, AtomicInteger> getCellRequests() {
      return _cellRequests;
    }
  }

  /**
   * Run a largestBlock search and check that the number of calls to getCell() is only equal to
   * the number of neighbours + 1.
   */
  @Test
  public void testNumberOfCalls() throws MalformedBlockException {
    final GridShim gridShim = new GridShim(
        " A C A F F F F ",
        " A C C C C E E ",
        " A D D D D D D ", // Ds form loops.
        " B B D A D A D ",
        " B B D D D D D "
    );
    final Set<Cell> largestBlock = gridShim.largestBlock();
    Assert.assertEquals("D", largestBlock.iterator().next().getValue());
    Assert.assertEquals(14, largestBlock.size());


    final Map<Cell, AtomicInteger> cellRequests = gridShim.getCellRequests();
    Assert.assertEquals(gridShim.width() * gridShim.height(), cellRequests.size()); // Check that we have recorded access to every cell.

    for (final Entry<Cell, AtomicInteger> entry : cellRequests.entrySet()) {
      final AtomicInteger calls = entry.getValue();
      final Cell cell = entry.getKey();


      if (cell.getX() == 0 || cell.getX() == gridShim.width() - 1) {
        if (cell.getY() == 0 || cell.getY() == gridShim.height() - 1) {
          Assert.assertTrue(calls.get() == 3); // Corner cell; 2 neighbours;
        }
        else {
          Assert.assertTrue(calls.get() == 4); // Edge cell; 3 neighbours;
        }
      }
      else if (cell.getY() == 0 || cell.getY() == gridShim.height() - 1) {
        Assert.assertTrue(calls.get() == 4); // Edge cell; 3 neighbours;
      }
      else {
        Assert.assertTrue(calls.get() == 5); // No cell should be requested more than 5 times (Direct get + as a neighbour for each adjacent cell)
      }
    }
  }

}
