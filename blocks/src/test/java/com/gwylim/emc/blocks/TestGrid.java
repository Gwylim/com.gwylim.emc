package com.gwylim.emc.blocks;

import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class TestGrid {

  private Grid _grid;

  @Before
  public void setup() throws MalformedBlockException {
    _grid = new Grid(
        " A C A F F F F ",
        " A C C C C E E ",
        " A D D A C E D ",
        " B B A B A A D ",
        " B B B B B D D "
    );
  }


  @Test
  public void testTask() throws Exception {
    final Set<Cell> largestBlock = _grid.largestBlock();
    Assert.assertEquals(8, largestBlock.size());
  }

  @Test
  public void testSize() throws Exception {
    Assert.assertEquals(7, _grid.width());
    Assert.assertEquals(5, _grid.height());
  }

  @Test
  public void testValue() throws Exception {
    Assert.assertEquals("A", _grid.value(0, 0));
    Assert.assertEquals("F", _grid.value(6, 0));
    Assert.assertEquals("B", _grid.value(0, 4));
    Assert.assertEquals("D", _grid.value(6, 4));
  }

  @Test
  public void testCellValue() throws Exception {
    final Cell cell = _grid.getCell(3, 2);
    Assert.assertEquals("A", cell.getValue());
  }

  @Test
  public void testNeighboursMiddle() throws Exception {
    final Cell cell = _grid.getCell(3, 2);
    final Set<Cell> neighbours = cell.getNeighbours();
    Assert.assertEquals(4, neighbours.size());

    Assert.assertTrue(neighbours.contains(_grid.getCell(3, 3)));
    Assert.assertTrue(neighbours.contains(_grid.getCell(3, 1)));

    Assert.assertTrue(neighbours.contains(_grid.getCell(2, 2)));
    Assert.assertTrue(neighbours.contains(_grid.getCell(4, 2)));
  }

  @Test
  public void testNeighboursCorner() throws Exception {
    final Cell cell = _grid.getCell(0, 0);
    final Set<Cell> neighbours = cell.getNeighbours();
    Assert.assertEquals(2, neighbours.size());

    Assert.assertTrue(neighbours.contains(_grid.getCell(0, 1)));
    Assert.assertTrue(neighbours.contains(_grid.getCell(1, 0)));
  }

  @Test
  public void testBlockSingle() throws Exception {
    final Cell cell = _grid.getCell(3, 2);
    final Set<Cell> block = cell.getBlock();
    Assert.assertEquals(1, block.size());
    Assert.assertTrue(block.contains(_grid.getCell(3, 2)));
  }

  @Test
  public void testBlockMulti() throws Exception {
    final Cell cell = _grid.getCell(2, 2);
    final Set<Cell> block = cell.getBlock();
    Assert.assertEquals(2, block.size());
    Assert.assertTrue(block.contains(_grid.getCell(2, 2)));
    Assert.assertTrue(block.contains(_grid.getCell(1, 2)));
  }
}
