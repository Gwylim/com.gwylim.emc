package com.gwyim.emc.robots.messaging;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * A server that responds to requests.
 * It should doso in a reasonable time as clients are are "{@link SynchronousClient}s" and will be blocked until the response is received.
 *
 * @param <RequestType>  The type that will be received as a request.
 * @param <ResponseType> The type that will be returned to the caller.
 *
 * @author Gwylim
 */
public class SynchronousServer<RequestType, ResponseType> {

  private final MessageSystem _messageSystem;
  private PushSubscription<SyncoronousRequest> _subscription;

  /**
   * Create a {@link SynchronousServer} that receives the given class as a request object then responds having computed the return value using the supplied function.
   * @param messageSystem The {@link MessageSystem} to subscribe to and listen to requests from.
   * @param requestClass  The class we expect to receive as a request.
   * @param name          The name of the server, this will be used by clients to address requests.
   * @param function      The function to calculate the response from the request.
   */
  SynchronousServer(final MessageSystem messageSystem, final Class<RequestType> requestClass, final String name, final Function<RequestType, ResponseType> function) {
    _messageSystem = messageSystem;

    _subscription = new PushSubscription<SyncoronousRequest>(SyncoronousRequest.class, new Predicate<SyncoronousRequest>() {
      @Override
      public boolean test(final SyncoronousRequest request) {
        return requestClass.isAssignableFrom(request.getData().getClass()) && request.getTarget().equals(name);
      }
    }, new Consumer<SyncoronousRequest>() {
      @Override
      public void accept(final SyncoronousRequest request) {
        final RequestType requestObject = requestClass.cast(request.getData());
        final ResponseType result = function.apply(requestObject);

        _messageSystem.publish(new SynchronousResponse(result, request.getReturnAddress()));
      }
    });

    _messageSystem.subscribe(_subscription);
  }

  /**
   * Stop the server and remove the subscription from the {@link MessageSystem} so no new requests will be received.
   * NB: Removing a server that clients are still making requests to will block them forever.
   */
  public void shutdown() {
    _messageSystem.unsubscribe(_subscription);
  }
}
