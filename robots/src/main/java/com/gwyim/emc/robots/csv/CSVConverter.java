package com.gwyim.emc.robots.csv;

/**
 * Converter for creating objects from CSV lines.
 *
 * @param <T> The type that the CSV lines are converted into.
 *
 * @author Gwylim
 */
@FunctionalInterface
public interface CSVConverter<T> {

  /**
   * Convert the input CSV line into an object.
   *
   * @param variables The CSV line components in line order.
   * @return A new object created from the CVS line data.
   */
  T convert(String[] variables);
}
