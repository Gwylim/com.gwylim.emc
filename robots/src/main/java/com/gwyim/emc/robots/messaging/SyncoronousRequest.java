package com.gwyim.emc.robots.messaging;

/**
 * A message object used to make a synchronous request to a {@link com.gwyim.emc.robots.messaging.sync.SynchronousServer}.
 *
 * public only for testing.
 *
 * @author Gwylim
 */
public final class SyncoronousRequest {

  private final String _target;
  private final Object _returnAddress;
  private final Object _data;

  /**
   * Construct a new request to the given target with the supplied data and return address.
   * @param target        The target service which this request should be delivered to.
   * @param data          The data for the request.
   * @param returnAddress The return address to which the response should be sent.
   */
  SyncoronousRequest(final String target, final Object data, final Object returnAddress) {
    _target = target;
    _data = data;
    _returnAddress = returnAddress;
  }

  /** The target service of this request. */
  public String getTarget() { return _target; }
  public Object getData() { return _data; }
  /** The address that responses should be sent to. */
  public Object getReturnAddress() { return _returnAddress; }


  @Override
  public String toString() {
    return "SyncoronousRequest [_target=" + _target + ", _returnAddress=" + _returnAddress + ", _data type=" + _data.getClass().getSimpleName() + "]";
  }
}