package com.gwyim.emc.robots.instructions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.gwyim.emc.robots.Robot;
import com.gwyim.emc.robots.map.Location;

/**
 * Instruction telling the receiving {@link Robot} to move to a specified {@link Location}.
 *
 * @author Gwylim
 */
public class MoveInstruction implements Instruction {
  private static final String DATE_FORMAT = "yyyy-MM-dd H:mm:ss";

  private final String _robot;
  private final Location _location;
  private final Date _datetime;

  /**
   * Create a {@link MoveInstruction} for a specified robot telling to to move to a specific {@link Location}.
   * @param robot     The {@link Robot} to instruct.
   * @param location  The location to move to.
   * @param datetime  TODO: unknown.
   */
  public MoveInstruction(final String robot, final Location location, final Date datetime) {
    _robot = robot;
    _datetime = new Date(datetime.getTime());
    _location = location;
  }

  /**
   * Create a {@link MoveInstruction} for a specified robot telling to to move to a specific {@link Location}.
   * Parse the datetime from a {@link String}.
   * @param robot     The {@link Robot} to instruct.
   * @param location  The location to move to.
   * @param datetime  TODO: unknown.
   */
  public MoveInstruction(final String robot, final Location location, final String datetime) {
    this(robot, location, parseDate(datetime));
  }

  /**
   * Parse a {@link Date} from a String provided in the format found in the data files.
   *
   * Note: {@link SimpleDateFormat} is NOT thread safe so a new one is created each time.
   * TODO: Synchronisation could be added it avoid recreation... though that may lead to contention.
   *
   * @param datetime  The date time {@link String} to parse.
   * @return  The Date parsed form the string.
   */
  public static Date parseDate(final String datetime) {
    try {
      return new SimpleDateFormat(DATE_FORMAT).parse(datetime); // Not thread safe.
    }
    catch (final ParseException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public String getRobot() { return _robot; }
  public Location getLocation() { return _location; }
  @Override
  public Date getDatetime() { return new Date(_datetime.getTime()); }

  @Override
  public void execute(final Robot r) {
    r.moveTo(_location, _datetime);
  }

  @Override
  public String toString() {
    return "Move [" + _robot + ", to " + _location + ", at " + _datetime + "]";
  }
}
