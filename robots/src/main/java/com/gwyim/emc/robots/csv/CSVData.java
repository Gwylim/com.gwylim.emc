package com.gwyim.emc.robots.csv;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Iterator;

import com.opencsv.CSVReader;

/**
 * Read the content of a CSV file as an {@link Iterator} for a specified type.
 *
 * OpenCSV has the facility to read into beans but this requires one to expose setters on objects that should be
 * immutable.
 * These could be hidden in most cases by using interfaces but this is less clean.
 *
 * @param <T> The type that lines are converted into.
 *
 * @author Gwylim
 */
public class CSVData<T> implements Iterable<T> {
  private final File _dataFile;
  private final CSVConverter<T> _converter;

  /**
   * Create a CSV data object backed by the given file and converted using the given {@link Converter}.
   * @param dataFile  The {@link File} to read data from.
   * @param converter The {@link Converter} to use to convert lines into objects.
   */
  public CSVData(final File dataFile, final CSVConverter<T> converter) {
    _dataFile = dataFile;
    _converter = converter;
  }

  @Override
  public Iterator<T> iterator() {
    try {
      return new CSVIterator();
    }
    catch (final FileNotFoundException e) {
      throw new RuntimeException("CSV file did not exist: " + _dataFile, e);
    }
  }

  /**
   * Iterate over the lines in the file converting each to an object.
   * @author Gwylim
   */
  private final class CSVIterator implements Iterator<T> {

    private final CSVReader _csv;
    private final Iterator<String[]> _iterator;

    /**
     * Create a CSVIterator using fields from the encapsulating class.
     * @throws FileNotFoundException If the {@link File} could not be found.
     */
    public CSVIterator() throws FileNotFoundException {
      _csv = new CSVReader(new InputStreamReader(new FileInputStream(_dataFile), Charset.forName("UTF-8")));
      _iterator = _csv.iterator();
    }

    @Override
    public boolean hasNext() {
      return _iterator.hasNext();
    }

    @Override
    public T next() {
      return _converter.convert(_iterator.next());
    }

    @Override
    protected void finalize() throws Throwable {
      _csv.close();
    }
  }
}
