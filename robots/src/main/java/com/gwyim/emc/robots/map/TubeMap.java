package com.gwyim.emc.robots.map;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gwyim.emc.robots.csv.CSVData;
import com.gwyim.emc.robots.messaging.MessageSystem;

/**
 * A map/collection of the tube stations and associated methods.
 *
 * @author Gwylim
 */
public class TubeMap {

  public static final String SERVER_NAME = "tubeMap";

  private static final Logger LOG = LogManager.getLogger(TubeMap.class);

  private final List<TubeStation> _stations = new ArrayList<TubeStation>();


  /**
   * Read a CSV data file into the map.
   * @param tubeMapDataFile The data file to use.
   * @param messageSystem
   * @throws IOException  If there is an issue reading the data file.
   */
  public TubeMap(final File tubeMapDataFile, final MessageSystem messageSystem) throws IOException {
    final CSVData<TubeStation> csvData = new CSVData<TubeStation>(tubeMapDataFile, v -> new TubeStation(v[0], new Location(v[1], v[2])));

    for (final TubeStation tubeStation : csvData) {
      _stations.add(tubeStation);
    }

    messageSystem.createSynchronousServer(SERVER_NAME, Location.class, new Function<Location, TubeStation>() {
      @Override
      public TubeStation apply(final Location location) {
        final TubeStation closestStation = getClosestStation(location);
        LOG.info("Got request for station closest to " + location + ", returning " + closestStation);
        return closestStation;
      }
    });
  }

  /**
   * Get all the stations on the map.
   */
  public List<TubeStation> getStations() {
    return _stations;
  }

  /**
   * Get the station closest to the given location.
   *
   * This is currently O(n) on a reasonable amount of math and can probably be done more efficiently.
   *
   * @param location The location to check closest station for.
   * @return  The closest station. Null iff there are no stations on the map.
   */
  private TubeStation getClosestStation(final Location location) {
    double closestDistance = Double.MAX_VALUE;
    TubeStation closestStation = null;

    for (final TubeStation tubeStation : _stations) {
      final double distance = location.distance(tubeStation.getLocation());

      if (closestDistance > distance) {
        closestDistance = distance;
        closestStation = tubeStation;
      }
    }

    return closestStation;
  }
}
