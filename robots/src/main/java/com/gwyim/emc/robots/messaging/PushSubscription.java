package com.gwyim.emc.robots.messaging;

import java.util.function.Consumer;
import java.util.function.Predicate;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * A subscription to a messaging system that will receive objects of a certain type.
 * Extend this class with an implementation of {@link #recieve(Object)} that will be called with each message.
 *
 * @param <T> The type that we are interested in receiving.
 *
 * @author Gwylim
 */
public class PushSubscription<T> extends Subscription<T> {

  private static final Logger LOG = LogManager.getLogger(SingleThreadMessageSystem.class);

  private final Thread _postman;

  /**
   * Create a subscription for the specified class that only accepts all messages of that class (or subclass).
   */
  public PushSubscription(final Class<T> clazz, final Consumer<T> consumer) {
    this(clazz, t -> true, consumer);
  }

  /**
   * Create a subscription for the specified class that only accepts messages that pass the given filter.
   */
  public PushSubscription(final Class<T> clazz, final Predicate<T> filter, final Consumer<T> consumer) {
    super(clazz, filter);
    _postman  = new Thread() {
      @Override
      public void run() {
        try {
          while (!hasShutdown()) {
            final T message = take();
            try {
              consumer.accept(message);
            }
            catch (final Exception e) {
              LOG.error("Exception caught by postman thread.", e);
            }
          }
        }
        catch (final InterruptedException e) {
          LOG.debug("PushSubscription postman interupted; terminating.");
        }
        LOG.debug("PushSubscription postman terminated.");
      }
    };
    _postman.setDaemon(true);
    _postman.start();
  }

  @Override
  void postShutdown() throws InterruptedException {
    _postman.interrupt(); // terminate the postman once shutdown is complete.
    _postman.join();
  }
}
