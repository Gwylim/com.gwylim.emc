package com.gwyim.emc.robots.messaging;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Predicate;


/**
 * A subscription to a messaging system that will receive objects of a certain type.
 *
 * @param <T> The type that we are interested in receiving.
 *
 * @author Gwylim
 */
abstract class Subscription<T> {

  private final BlockingQueue<T> _queue = new LinkedBlockingQueue<T>();

  private final Class<T> _clazz;

  /** Set to true once shutdown has been requested and the queue emptied. */
  private volatile boolean _hasShutdown = false;

  /** Filter to only the messages we are interested in. */
  private Predicate<T> _filter;

  /**
   * Create a subscription for the specified class.
   */
  public Subscription(final Class<T> clazz) {
    this(clazz, t -> true); // Default to accepting all messages of the correct type.
  }

  /**
   * Create a subscription for the specified class.
   */
  public Subscription(final Class<T> clazz, final Predicate<T> filter) {
    _clazz = clazz;
    _filter = filter;
  }

  /**
   * Add an object to the subscription, iff it is a subclass of the intended type.
   * @param o The object to attempt to add.
   * @return  <code>true</code> if it is added, false otherwise.
   */
  @edu.umd.cs.findbugs.annotations.SuppressWarnings("JLM_JSR166_UTILCONCURRENT_MONITORENTER") // We are synchronising on a java.util.concurrent for more than just its methods.
  boolean add(final Object o) {
    synchronized (_queue) {
      if (_clazz.isAssignableFrom(o.getClass()) && !hasShutdown()) { // Accept only appropriate types and reject once we have shutdown.
        final T cast = _clazz.cast(o);
        if (_filter.test(cast)) {
          _queue.add(cast);
          return true;
        }
        else {
          return false;
        }
      }
      return false;
    }

  }


  /**
   * Get the next message object, waiting as required.
   * @return  The next message Object.
   * @throws InterruptedException If the thread is interrupted while waiting.
   */
  T take() throws InterruptedException {
    return _queue.take();
  }

  /**
   * Block until all messages have been processed.
   * NOTE: This will block add() until it terminates at which point the add will fail.
   * @throws InterruptedException If the wait for shutdown is interrupted.
   */
  @edu.umd.cs.findbugs.annotations.SuppressWarnings("JLM_JSR166_UTILCONCURRENT_MONITORENTER") // We are synchronising on a java.util.concurrent for more than just its methods.
  void shutdown() throws InterruptedException {
    synchronized (_queue) {
      while (!_queue.isEmpty()) {
        Thread.yield();
      }
      _hasShutdown = true;
      postShutdown();
    }
  }

  /**
   * Override this to do any post shutdown cleanup.
   * The message queue will be empty and hasShutdown() will return true.
   * @throws InterruptedException
   */
  void postShutdown() throws InterruptedException { }

  /**
   * Has this subscription shutdown.
   */
  boolean hasShutdown() {
    return _hasShutdown;
  }

}
