package com.gwyim.emc.robots.report;

import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gwyim.emc.robots.messaging.MessageSystem;
import com.gwyim.emc.robots.messaging.PushSubscription;

/**
 * A receiver of traffic reports.
 * @author Gwylim
 */
public class TrafficReportReciever {

  private static final Logger LOG = LogManager.getLogger(TrafficReportReciever.class);

  private final Set<TrafficReport> _reports = new LinkedHashSet<TrafficReport>();

  /**
   * Create a new {@link TrafficReportReciever} listening to the given {@link MessageSystem}.
   */
  public TrafficReportReciever(final MessageSystem messageSystem) {
    final PushSubscription<TrafficReport> subscription = new PushSubscription<TrafficReport>(TrafficReport.class, report -> addReport(report));
    messageSystem.subscribe(subscription);
  }

  /**
   * Add a new report.
   */
  public synchronized void addReport(final TrafficReport report) {
    _reports.add(report);
    LOG.info("Traffic report recieved: " + report + " there are now " + _reports.size() + " reports present.");
  }

  /**
   * Get all the reports currently stored.
   * This is a snapshot of the reports a the time the request was made and will not be updated.
   */
  public synchronized Set<TrafficReport> currentReports() {
    return new LinkedHashSet<TrafficReport>(_reports);
  }

}
