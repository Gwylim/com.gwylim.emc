package com.gwyim.emc.robots.report;

import java.util.Date;

/**
 * A traffic report recording the source robot name, the time, speed, {@link TrafficCondition}.
 *
 * NOTE: Location is omitted for some reason?
 *
 * @author Gwylim
 */
public class TrafficReport {

  private final String _robot;
  private final Date _dateTime;
  private final double _speed;
  private final TrafficCondition _trafficCondition;

  /**
   * Create a new {@link TrafficReport}.
   * @param robot       The name of the robot that made the report.
   * @param dateTime    The datetime that the report was made.
   * @param speed       The speed that the robot was travelling.
   * @param trafficCondition  The conditions of the traffic at the time of the report.
   */
  public TrafficReport(final String robot, final Date dateTime, final double speed, final TrafficCondition trafficCondition) {
    _robot = robot;
    _dateTime = new Date(dateTime.getTime());
    _speed = speed;
    _trafficCondition = trafficCondition;
  }

  public String getRobot() { return _robot; }
  public Date getDateTime() { return new Date(_dateTime.getTime()); }
  public double getSpeed() { return _speed; }
  public TrafficCondition getTrafficCondition() { return _trafficCondition; }


  @Override
  public String toString() {
    return "At " + _dateTime + ", " + _robot + " reports speed of " + _speed + " in " + _trafficCondition.name() + " traffic.";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + (_dateTime == null ? 0 : _dateTime.hashCode());
    result = prime * result + (_robot == null ? 0 : _robot.hashCode());
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) { return true; }
    if (obj == null) { return false; }
    if (getClass() != obj.getClass()) { return false; }
    final TrafficReport other = (TrafficReport) obj;
    if (_dateTime == null) {
      if (other._dateTime != null) { return false; }
    }
    else if (!_dateTime.equals(other._dateTime)) { return false; }
    if (_robot == null) {
      if (other._robot != null) { return false; }
    }
    else if (!_robot.equals(other._robot)) { return false; }
    return true;
  }
}
