package com.gwyim.emc.robots.instructions;

import java.util.Date;

import com.gwyim.emc.robots.Robot;

/**
 * An instruction sent to a robot by the dispatcher.
 * @author Gwylim
 */
public interface Instruction {

  /**
   * Execute the command given the robot.
   * This is a callback from the robot so subclasses can perform required actions on the {@link Robot} object.
   *
   * @param r The robot that received the {@link Instruction}.
   */
  void execute(Robot r);

  /**
   * The intended recipient of the instruction.
   * @return the name of the intended {@link Robot}.
   */
  String getRobot();

  /**
   * Get the time that the instruction was issued.
   */
  Date getDatetime();
}
