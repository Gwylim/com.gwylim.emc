package com.gwyim.emc.robots;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gwyim.emc.robots.instructions.Instruction;
import com.gwyim.emc.robots.map.Location;
import com.gwyim.emc.robots.map.TubeMap;
import com.gwyim.emc.robots.map.TubeStation;
import com.gwyim.emc.robots.messaging.MessageSystem;
import com.gwyim.emc.robots.messaging.PullSubscription;
import com.gwyim.emc.robots.messaging.SynchronousClient;
import com.gwyim.emc.robots.report.TrafficCondition;
import com.gwyim.emc.robots.report.TrafficReport;

/**
 * The representation of a {@link Robot} in the simulation.
 *
 * @author Gwylim
 */
public class Robot extends Thread {

  private static final Logger LOG = LogManager.getLogger(Robot.class);

  private static final int ONE_SECOND_IN_MS = 1000;
  private static final int STATION_PROXIMITY_M = 350;

  /**
   * The current location of the robot.
   */
  private Location _location;

  /**
   * Time of arrival a the current location.
   */
  private Date _arrivalTime;

  /**
   * Has the robot been requested to stop execution an terminate.
   */
  private boolean _stop;

  private final MessageSystem _messageSystem;

  private final SynchronousClient<Location, TubeStation> _stationLookupClient;

  private final PullSubscription<InstructionBatch> _instructionSubscription;


  /**
   * Construct a robot with the specified name and a {@link MessageSystem} with which to communicate.
   * @param name  The name of the robot.
   * @param messageSystem The {@link MessageSystem} used to send and receive messages.
   */
  public Robot(final String name, final MessageSystem messageSystem) {
    super(name);
    _messageSystem = messageSystem;
    _stationLookupClient = _messageSystem.createSynchronousClient(TubeMap.SERVER_NAME, TubeStation.class);

    _instructionSubscription = new PullSubscription<InstructionBatch>(
        InstructionBatch.class,                                          // Receive all instructions.
        batch -> batch.getRobot().equals(getName())     // That are intended for us.
    );

    _messageSystem.subscribe(_instructionSubscription);

    LOG.info("Robot " + name + " created.");
  }

  @Override
  public void run() {

    try {
      do {
        final InstructionBatch instructionBatch = _instructionSubscription.recieve();

        for (final Instruction instruction : instructionBatch.getInstructions()) {
          LOG.info("Running: " + instruction);
          instruction.execute(this);
        }
      } while (!_stop);

      LOG.info("Robot terminated.");

    }
    catch (final InterruptedException e) {
      LOG.error("Interuption recieved, terminating simulation.");
      Thread.interrupted();
    }
    finally {
      _messageSystem.unsubscribe(_instructionSubscription);
    }
  }

  /**
   * Move to the specified location and take a reading if appropriate.
   * @param location  The location to move to.
   * @param datetime  The time to arrive at the location.
   */
  public void moveTo(final Location location, final Date datetime) {
    if (_location == null) {
      // First instruction; Assume this defines the first location.
      _location = location;
      _arrivalTime = new Date(datetime.getTime());
    }
    else {
      // Not first instruction; Move to new location.
      final double distance = _location.distance(location);
      final double msToNextLocation = datetime.getTime() - _arrivalTime.getTime();

      final double speed = distance / (msToNextLocation / ONE_SECOND_IN_MS);

      LOG.info("Moved to " + location + " distance is " + distance + " speed was " + speed + " m/s.");
      _location = location;
      _arrivalTime = new Date(datetime.getTime());

      final TubeStation closestStation = _stationLookupClient.request(location);

      final Location cloasestStationLocation = closestStation.getLocation();
      if (_location.distance(cloasestStationLocation) < STATION_PROXIMITY_M) {
        LOG.info("Near '" + closestStation.getName() + "' recording traffic...");
        final TrafficReport trafficReport = new TrafficReport(getName(), _arrivalTime, speed, TrafficCondition.random());
        _messageSystem.publish(trafficReport);
      }
    }

  }

  /**
   * Request that this robot terminates.
   * The thread running this robot will end.
   */
  public void terminate() {
    LOG.info("recieved instruction to terminate.");
    _stop = true;
  }

}
