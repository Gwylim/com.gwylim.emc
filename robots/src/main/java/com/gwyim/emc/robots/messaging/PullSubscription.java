package com.gwyim.emc.robots.messaging;

import java.util.function.Predicate;


/**
 * A subscription to a messaging system that will receive objects of a certain type, receive has to be called to get each message.
 *
 * @param <T> The type that we are interested in receiving.
 *
 * @author Gwylim
 */
public class PullSubscription<T> extends Subscription<T> {

  /**
   * Create a subscription for the specified class.
   */
  public PullSubscription(final Class<T> clazz) {
    super(clazz);
  }

  /**
   * Create a subscription for the specified class with the specified filter.
   */
  public PullSubscription(final Class<T> clazz, final Predicate<T> filter) {
    super(clazz, filter);
  }

  /**
   * Take a subscription item from the Queue, blocking until one is available.
   * @return  The object.
   * @throws InterruptedException if an interruption occurs while waiting.
   */
  public T recieve() throws InterruptedException {
    return take();
  }
}
