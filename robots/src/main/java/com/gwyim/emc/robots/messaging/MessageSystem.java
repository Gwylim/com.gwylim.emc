package com.gwyim.emc.robots.messaging;

import java.util.function.Function;


/**
 * Implements a messaging system with the ability to publish and receive.
 *
 * @author Gwylim
 */
public interface MessageSystem {

  /**
   * Publish an object that will be receivable by all subscribers that wish for classes that the Object can be cast to.
   */
  void publish(Object o);

  /**
   * Subscribe to receive objects.
   */
  void subscribe(Subscription<?> s);

  /**
   * Remove the given {@link Subscription} from the {@link MessageSystem}.
   */
  void unsubscribe(Subscription<?> subscription);

  /**
   * Create a new SynchronousClient on this {@link MessageSystem}.
   * @param target        The target {@link SynchronousServer}.
   * @param responseClass The response type.
   * @return              A new {@link SynchronousClient}
   */
  <RequestType, ResponseType> SynchronousClient<RequestType, ResponseType> createSynchronousClient(final String target, final Class<ResponseType> responseClass);

  /**
   * Create a new {@link SynchronousServer} on this {@link MessageSystem}.
   * @param name          The name of the server.
   * @param requestClass  The requests it receives.
   * @param function      The function it uses to calculate the response.
   * @return              A new {@link SynchronousClient}.
   */
  <RequestType, ResponseType> SynchronousServer<RequestType, ResponseType> createSynchronousServer(final String name, final Class<RequestType> requestClass, final Function<RequestType, ResponseType> function);

  /**
   * Terminate the {@link MessageSystem}, ending any threads it may be running.
   * Any remaining messages will be delivered but no new messages may be added.
   * This will block until all messages have been delivered.
   * @throws InterruptedException
   */
  void shutdown() throws InterruptedException;

}
