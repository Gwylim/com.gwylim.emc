package com.gwyim.emc.robots.map;


/**
 * Representation of a tube station.
 * Location and name.
 *
 * @author Gwylim
 */
public class TubeStation {
  private final String _name;
  private final Location _location;

  /**
   * Create a {@link TubeStation} with the given name and location.
   * @param name      The name of the station.
   * @param location  The location of the station.
   */
  public TubeStation(final String name, final Location location) {
    _name = name;
    _location = location;
  }

  public String getName() { return _name; }
  public Location getLocation() { return _location; }

  @Override
  public String toString() {
    return "TubeStation [" + _name + ": " + _location + "]";
  }
}
