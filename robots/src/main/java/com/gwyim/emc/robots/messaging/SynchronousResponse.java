package com.gwyim.emc.robots.messaging;

/**
 * A response from a {@link com.gwyim.emc.robots.messaging.sync.SynchronousServer} back to the client.
 *
 * public only for testing.
 *
 * @author Gwylim
 */

public final class SynchronousResponse {
  private final Object _result;
  private final Object _returnAddress;

  /**
   * Create a response with the given result to the given return address.
   * @param result        The result of the synchronous call.
   * @param returnAddress The subscriber to return the result to.
   */
  SynchronousResponse(final Object result, final Object returnAddress) {
    _result = result;
    _returnAddress = returnAddress;
  }

  /** The result of the request. */
  public Object getResult() { return _result; }
  /** The address to which this response should be delivered. */
  public Object getAddress() { return _returnAddress; }

  @Override
  public String toString() {
    return "SynchronousResponse [_result type=" + _result.getClass().getSimpleName() + ", _returnAddress=" + _returnAddress + "]";
  }
}
