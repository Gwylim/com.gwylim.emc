package com.gwyim.emc.robots.report;

import java.util.Random;

/**
 * The conditions of the traffic.
 *
 * @author Gwylim
 */
public enum TrafficCondition {
  HEAVY, LIGHT, MODERATE;

  private static final Random RANDOM = new Random();

  /**
   * Get a random {@link TrafficCondition}.
   */
  public static TrafficCondition random()  {
    return values()[RANDOM.nextInt(values().length)];
  }

}
