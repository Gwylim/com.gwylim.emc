package com.gwyim.emc.robots;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gwyim.emc.robots.instructions.Instruction;
import com.gwyim.emc.robots.instructions.StopInstruction;
import com.gwyim.emc.robots.messaging.MessageSystem;

/**
 * A component of the dispatcher that is responsible for sending instructions to a single {@link Robot}.
 * @author Gwylim
 */
public class RobotSubDispatcher implements Callable<Void> {

  private static final Logger LOG = LogManager.getLogger(RobotSubDispatcher.class);

  private static final int ROBOT_INSTRUCTION_BATCH_SIZE = 10;

  private final String _robot;
  private final Iterable<Instruction> _instructions;

  private final Date _endTime;

  private final MessageSystem _messageSystem;

  /**
   * Create a {@link RobotSubDispatcher} for the given {@link Robot} and {@link Instruction}s.
   * @param robot
   * @param instructions
   * @param messageSystem
   */
  public RobotSubDispatcher(final String robot, final Iterable<Instruction> instructions, final Date endTime, final MessageSystem messageSystem) {
    _robot = robot;
    _instructions = instructions;
    _messageSystem = messageSystem;
    _endTime = new Date(endTime.getTime());
  }

  /**
   * Send batches of messages to the {@link Robot} until the source of messages is exhausted.
   * Stop before any instruction that is after the end time.
   */
  @Override
  public Void call() throws Exception {
    LOG.info("Statring dispatch for robot " + _robot);

    final Iterator<Instruction> instructonIterator = _instructions.iterator();

    while (instructonIterator.hasNext()) { // For all the instructions.

      int instructionInBatchCount = 0;
      final List<Instruction> batch = new ArrayList<Instruction>(ROBOT_INSTRUCTION_BATCH_SIZE);

      while (instructonIterator.hasNext() && instructionInBatchCount++ < ROBOT_INSTRUCTION_BATCH_SIZE) { // Batch into groups.
        final Instruction nextInstruction = instructonIterator.next();

        if (_endTime.after(nextInstruction.getDatetime())) { // Next instruction is after the end time.
          batch.add(nextInstruction);
        }
        else {
          LOG.info("Time limit (" + _endTime + ") exceeded for " + _robot + " sending STOP instruction.");
          batch.add(new StopInstruction(_robot, new Date(_endTime.getTime())));
          _messageSystem.publish(new InstructionBatch(batch, _robot));
          return null; // Terminate RobotSubDispatcher.
        }
      }

      _messageSystem.publish(new InstructionBatch(batch, _robot));
    }

    LOG.info("No more instructions for " + _robot + " sending STOP instruction.");
    _messageSystem.publish(new StopInstruction(_robot, new Date(_endTime.getTime())));

    return null;
  }
}
