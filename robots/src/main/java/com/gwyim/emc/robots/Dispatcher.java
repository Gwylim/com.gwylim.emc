package com.gwyim.emc.robots;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gwyim.emc.robots.instructions.Instruction;
import com.gwyim.emc.robots.messaging.MessageSystem;

/**
 * The dispatcher that handles sending instructions to the {@link Robot}s.
 * Sending and iteration of message input is delegated to {@link RobotSubDispatcher}s.
 *
 * @author Gwylim
 */
public class Dispatcher {

  private static final Logger LOG = LogManager.getLogger(Dispatcher.class);

  private final Map<Iterable<Instruction>, String> _dataToRobot;

  private final Date _endDate;

  private final MessageSystem _messageSystem;

  /**
   * Create a {@link Dispatcher} for the provided {@link Map} of instructions to {@link Robot}.
   * @param dataToRobot The mapping between instructions and target robot.
   * @param messageSystem
   */
  public Dispatcher(final Map<Iterable<Instruction>, String> dataToRobot, final Date endDate, final MessageSystem messageSystem) {
    _dataToRobot = dataToRobot;
    _messageSystem = messageSystem;
    _endDate = new Date(endDate.getTime());
  }

  /**
   * Perform the dispatching. Create {@link RobotSubDispatcher}s and execute them, then wait for them to terminate.
   *
   * @throws InterruptedException If an interruption is caught while waiting for the {@link RobotSubDispatcher}s to terminate.
   * @throws ExecutionException If there is an error while running one of the {@link RobotSubDispatcher}s.
   */
  public void dispatch() throws InterruptedException, ExecutionException {
    final ExecutorService executor = Executors.newCachedThreadPool();

    final List<Future<Void>> tasks = new ArrayList<Future<Void>>(_dataToRobot.entrySet().size());

    LOG.info("Creating dispatcher tasks.");
    for (final Entry<Iterable<Instruction>, String> entry : _dataToRobot.entrySet()) {
      final String robot = entry.getValue();
      final Iterable<Instruction> instructions = entry.getKey();

      tasks.add(executor.submit(new RobotSubDispatcher(robot, instructions, new Date(_endDate.getTime()), _messageSystem)));
    }

    LOG.info("Waliting for dispatcher tasks to complete.");
    for (final Future<Void> future : tasks) {
      future.get();
    }

    executor.shutdown();
  }


}
