package com.gwyim.emc.robots;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gwyim.emc.robots.csv.CSVData;
import com.gwyim.emc.robots.instructions.Instruction;
import com.gwyim.emc.robots.instructions.MoveInstruction;
import com.gwyim.emc.robots.map.Location;
import com.gwyim.emc.robots.map.TubeMap;
import com.gwyim.emc.robots.messaging.MessageSystem;
import com.gwyim.emc.robots.messaging.SingleThreadMessageSystem;
import com.gwyim.emc.robots.report.TrafficReport;
import com.gwyim.emc.robots.report.TrafficReportReciever;

/**
 * Main class for setting up and running the robot simulation.
 *
 * @author Gwylim
 */
public class Runner {

  private static final Logger LOG = LogManager.getLogger(Runner.class);

  /**
   * Run the simulation from the command line.
   * @param args  NONE.
   * @throws Exception When errors occur.
   */
  public static void main(final String[] args) throws Exception {
    runSimulation();
  }

  /**
   * Run the simulation directly.
   */
  public static Set<TrafficReport> runSimulation() throws Exception {
    final File tubeStationData = new File(TubeMap.class.getResource("tube.csv").toURI());
    final File instructionFile6043 = new File(Runner.class.getResource("6043.csv").toURI());
    final File instructionFile5937 = new File(Runner.class.getResource("5937.csv").toURI());

    final MessageSystem messageSystem = new SingleThreadMessageSystem();

    final TubeMap map = new TubeMap(tubeStationData, messageSystem);
    final TrafficReportReciever trafficReportReciever = new TrafficReportReciever(messageSystem);

    LOG.info(map.getStations());

    // CHECKSTYLE:OFF: MagicNumber
    final CSVData<Instruction> instructions6043 = new CSVData<Instruction>(instructionFile6043, v -> new MoveInstruction(v[0], new Location(v[1], v[2]), v[3]));
    final CSVData<Instruction> instructions5937 = new CSVData<Instruction>(instructionFile5937, v -> new MoveInstruction(v[0], new Location(v[1], v[2]), v[3]));
    // CHECKSTYLE:ON: MagicNumber

    final Robot robot6043 = new Robot("6043", messageSystem);
    final Robot robot5937 = new Robot("5937", messageSystem);

    robot5937.start();
    robot6043.start();

    final Map<Iterable<Instruction>, String> dataToRobot = new LinkedHashMap<Iterable<Instruction>, String>();
    dataToRobot.put(instructions6043, robot6043.getName());
    dataToRobot.put(instructions5937, robot5937.getName());

    final Dispatcher dispatcher = new Dispatcher(dataToRobot, MoveInstruction.parseDate("2011-3-22 08:10:00"), messageSystem);
    LOG.info("Dispatching instructions to robots.");
    dispatcher.dispatch();

    LOG.info("Waiting for robots to finish.");
    robot5937.join();
    robot6043.join();

    LOG.info("Waiting for remaining messages to be delivered.");
    messageSystem.shutdown();

    final Set<TrafficReport> reports = trafficReportReciever.currentReports();
    LOG.info("Total number of reports collected: " + reports.size());
    return reports;
  }

}
