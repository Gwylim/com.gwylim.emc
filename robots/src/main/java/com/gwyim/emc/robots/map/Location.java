package com.gwyim.emc.robots.map;

/**
 * A {@link Location} given by latitude and longitude.
 *
 * @author Gwylim
 */
public class Location {

  private static final double RADIUS_OR_EARTH = 6371000;

  private final double _latitude;
  private final double _longitude;

  /**
   * Create a location.
   * @param latitude   The latitude of the location.
   * @param longitude  The longitude of the location.
   */
  public Location(final double latitude, final double longitude) {
    _latitude = latitude;
    _longitude = longitude;
  }

  /**
   * Create a location from {@link String} arguments for latitude and longitude.
   * @param latitude   The latitude of the location.
   * @param longitude  The longitude of the location.
   */
  public Location(final String latitude, final String longitude) {
    this(Double.parseDouble(latitude), Double.parseDouble(longitude));
  }

  public double getLatitude() { return _latitude; }
  public double getLongitude() { return _longitude; }

  /**
   * Get the distance in meters between this location and the supplied other.
   *
   * Implementation comes from:
   * http://www.movable-type.co.uk/scripts/latlong.html
   */
  public double distance(final Location other) {
    final double lat1 = getLatitude();
    final double lat2 = other.getLatitude();
    final double lon2 = getLongitude();
    final double lon1 = other.getLongitude();

    final double lat1Rad = Math.toRadians(lat1);
    final double lat2Rad = Math.toRadians(lat2);
    final double latDiffRad = Math.toRadians(lat2 - lat1);
    final double lonDiffRad = Math.toRadians(lon2 - lon1);

    final double a = Math.sin(latDiffRad / 2) * Math.sin(latDiffRad / 2)
                     + Math.cos(lat1Rad) * Math.cos(lat2Rad)
                     * Math.sin(lonDiffRad / 2) * Math.sin(lonDiffRad / 2);

    final double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    return RADIUS_OR_EARTH * c;
  }

  @Override
  public String toString() {
    return "(" + _latitude + ", " + _longitude + ")";
  }

  // CHECKSTYLE:OFF: MagicNumber
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    long temp;
    temp = Double.doubleToLongBits(_latitude);
    result = prime * result + (int) (temp ^ temp >>> 32);
    temp = Double.doubleToLongBits(_longitude);
    result = prime * result + (int) (temp ^ temp >>> 32);
    return result;
  }
  // CHECKSTYLE:ON: MagicNumber

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) { return true; }
    if (obj == null) { return false; }
    if (getClass() != obj.getClass()) { return false; }
    final Location other = (Location) obj;
    if (Double.doubleToLongBits(_latitude) != Double.doubleToLongBits(other._latitude)) { return false; }
    if (Double.doubleToLongBits(_longitude) != Double.doubleToLongBits(other._longitude)) { return false; }
    return true;
  }
}
