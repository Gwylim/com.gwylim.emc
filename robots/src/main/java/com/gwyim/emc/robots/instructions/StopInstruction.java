package com.gwyim.emc.robots.instructions;

import java.util.Date;

import com.gwyim.emc.robots.Robot;

/**
 * {@link Instruction} telling the {@link Robot} to stop and terminate.
 *
 * @author Gwylim
 */
public class StopInstruction implements Instruction {

  private final String _robot;
  private final Date _dateTime;

  /**
   * Create a new {@link StopInstruction} for a specific {@link Robot}.
   * @param robot The robot to instruct.
   */
  public StopInstruction(final String robot, final Date dateTime) {
    _robot = robot;
    _dateTime = new Date(dateTime.getTime());
  }

  @Override
  public void execute(final Robot r) {
    r.terminate();
  }

  @Override
  public String getRobot() { return _robot; }

  @Override
  public Date getDatetime() {
    return new Date(_dateTime.getTime());
  }

  @Override
  public String toString() {
    return "STOP [" + _robot + ", at " + _dateTime + "]";
  }
}
