package com.gwyim.emc.robots.messaging;

import java.util.UUID;
import java.util.function.Predicate;

/**
 * A client to a named service on the {@link MessageSystem} that makes the request then blocks until a response has been received.
 *
 * @param <RequestType>  The type that will be received as a request.
 * @param <ResponseType> The type that will be returned to the caller.
 *
 * @author Gwylim
 */
public class SynchronousClient<RequestType, ResponseType> {

  private final Class<ResponseType> _responseClass;
  private final MessageSystem _messageSystem;
  private final String _target;

  /**
   * Create a {@link SynchronousClient} on the given {@link MessageSystem} with the specified target service and expected response class.
   * @param messageSystem
   * @param target
   * @param responseClass
   */
  SynchronousClient(final MessageSystem messageSystem, final String target, final Class<ResponseType> responseClass) {
    _messageSystem = messageSystem;
    _target = target;
    _responseClass = responseClass;
  }

  /**
   * Perform a request and wait for the response.
   * @param request The value used to make the request.
   * @return        The response value or <code>null</code> if interrupted.
   */
  public ResponseType request(final RequestType request) {
    final UUID returnAddress = UUID.randomUUID(); // Create a unique return address for the response.

    // Create subscription for the return value.
    final PullSubscription<SynchronousResponse> subscription = new PullSubscription<SynchronousResponse>(SynchronousResponse.class, new Predicate<SynchronousResponse>() {
      @Override
      public boolean test(final SynchronousResponse response) {
        // Filter out all responses not intended for this invocation by ID and return type.
        return response.getAddress().equals(returnAddress) && _responseClass.isAssignableFrom(response.getResult().getClass());
      }
    });
    _messageSystem.subscribe(subscription); // Subscribe BEFORE making request.

    // Send a request message to the target:
    _messageSystem.publish(new SyncoronousRequest(_target, request, returnAddress));

    try {
      // Await the response.
      final SynchronousResponse response = subscription.recieve();
      return _responseClass.cast(response.getResult());
    }
    catch (final InterruptedException e) {
      // We have been forced to terminate before getting a value; return null.
      return null;
    }
    finally {
      // Make sure we remove the throw away subscription.
      _messageSystem.unsubscribe(subscription);
    }
  }

}
