package com.gwyim.emc.robots;

import java.util.List;

import com.gwyim.emc.robots.instructions.Instruction;

/**
 * A batch of {@link Instruction}s for a robot.
 *
 * @author Gwylim
 */
public class InstructionBatch {

  private final List<Instruction> _instructions;
  private final String _robot;

  /**
   * Create a new batch of {@link Instruction}s for the robot with the supplied name.
   * @param instructions The batch of instructions.
   * @param robot The recipient robot.
   */
  public InstructionBatch(final List<Instruction> instructions, final String robot) {
    _instructions = instructions;
    _robot = robot;
  }

  /** The batch of {@link Instruction}s for the robot to follow. */
  public List<Instruction> getInstructions() { return _instructions; }

  /** The intended recipient robot. */
  public String getRobot() { return _robot; }
}
