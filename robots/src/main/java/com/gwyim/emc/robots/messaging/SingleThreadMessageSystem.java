package com.gwyim.emc.robots.messaging;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Function;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * A very simple messaging system.
 *
 * Subscribers are only guaranteed to receive messages published AFTER they are they subscribe.
 *
 * @author Gwylim
 */
public class SingleThreadMessageSystem implements MessageSystem {

  private static final Logger LOG = LogManager.getLogger(SingleThreadMessageSystem.class);

  private final BlockingQueue<Object> _inputQueue = new LinkedBlockingQueue<Object>();

  private final Set<Subscription<?>> _subscribers = new LinkedHashSet<Subscription<?>>();

  /** Set to true once shutdown has been requested and the queue emptied. */
  private volatile boolean _hasShutdown = false;

  private final Thread _postman;

  /**
   * Create a new {@link SingleThreadMessageSystem} and start its postman thread.
   */
  public SingleThreadMessageSystem() {
    _postman = new Thread() {
      @Override
      public void run() {
        try {
          while (!_hasShutdown) {
            final Object o = _inputQueue.take();

            boolean added = false;
            synchronized (_subscribers) {
              for (final Subscription<?> subscription : _subscribers) {
                added |= subscription.add(o);
              }
            }

            if (!added) {
              LOG.warn("No subscribers accepted " + o + " object discarded.");
            }
          }
        }
        catch (final InterruptedException e) {
          LOG.warn("SingleThreadMessageSystem postman recieved interupt; terminating.");
        }

        LOG.warn("SingleThreadMessageSystem postman terminated.");
      }
    };
    _postman.setDaemon(true);
    _postman.start();
  }

  @Override
  @edu.umd.cs.findbugs.annotations.SuppressWarnings("JLM_JSR166_UTILCONCURRENT_MONITORENTER") // We are synchronising on a java.util.concurrent for more than just its methods.
  public void publish(final Object o) {
    synchronized (_inputQueue) {
      if (!_hasShutdown) {
        _inputQueue.add(o);
      }
      else {
        throw new IllegalStateException("This MessageSystem has shutdown, no new messages can be added.");
      }
    }
  }

  @Override
  public void subscribe(final Subscription<?> s) {
    if (!_hasShutdown) {
      synchronized (_subscribers) {
        _subscribers.add(s);
      }
    }
    else {
      throw new IllegalStateException("This MessageSystem has shutdown, no new subscriptions can be added.");
    }
  }

  @Override
  public void unsubscribe(final Subscription<?> s) {
    synchronized (_subscribers) {
      _subscribers.remove(s);
    }
  }


  @Override
  public <RequestType, ResponseType> SynchronousClient<RequestType, ResponseType> createSynchronousClient(final String target, final Class<ResponseType> responseClass) {
    return new SynchronousClient<RequestType, ResponseType>(this, target, responseClass);
  }

  @Override
  public <RequestType, ResponseType> SynchronousServer<RequestType, ResponseType> createSynchronousServer(final String name, final Class<RequestType> requestClass, final Function<RequestType, ResponseType> function) {
    return new SynchronousServer<RequestType, ResponseType>(this, requestClass, name, function);
  }

  @Override
  @edu.umd.cs.findbugs.annotations.SuppressWarnings("JLM_JSR166_UTILCONCURRENT_MONITORENTER") // We are synchronising on a java.util.concurrent for more than just its methods.
  public void shutdown() throws InterruptedException {
    synchronized (_inputQueue) {
      while (!_inputQueue.isEmpty()) {
        Thread.yield();
      }
      _hasShutdown = true;
    }
    _postman.interrupt();

    synchronized (_subscribers) {
      for (final Subscription<?> subscription : _subscribers) {
        subscription.shutdown();
      }
    }
  }
}
