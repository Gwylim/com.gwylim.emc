package com.gwyim.emc.robots;

import java.util.Optional;
import java.util.Set;

import junit.framework.Assert;

import org.junit.Test;

import com.gwyim.emc.robots.report.TrafficReport;

/**
 * Regression test against known values.
 *
 * @author Gwylim
 */
public class RegressionTest {

  @Test
  public void test() throws Exception {
    final Set<TrafficReport> reports = Runner.runSimulation();

    Assert.assertEquals(478, reports.size());

    final Optional<Integer> reportBy6043 = reports.parallelStream().filter(r -> r.getRobot().equals("6043")).map(r -> 1).reduce((a, b) -> a + b);
    final Optional<Integer> reportBy5937 = reports.parallelStream().filter(r -> r.getRobot().equals("5937")).map(r -> 1).reduce((a, b) -> a + b);

    Assert.assertEquals(Integer.valueOf(286), reportBy6043.get());
    Assert.assertEquals(Integer.valueOf(192), reportBy5937.get());
  }
}
