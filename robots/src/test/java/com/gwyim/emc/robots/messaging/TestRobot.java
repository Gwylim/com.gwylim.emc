package com.gwyim.emc.robots.messaging;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;

import org.easymock.Capture;
import org.easymock.EasyMock;
import org.junit.Test;

import com.gwyim.emc.robots.InstructionBatch;
import com.gwyim.emc.robots.Robot;
import com.gwyim.emc.robots.instructions.MoveInstruction;
import com.gwyim.emc.robots.instructions.StopInstruction;
import com.gwyim.emc.robots.map.Location;
import com.gwyim.emc.robots.map.TubeMap;
import com.gwyim.emc.robots.map.TubeStation;
import com.gwyim.emc.robots.report.TrafficReport;

/**
 * Test the {@link Robot}s' interactions with the {@link MessageSystem}.
 * @author Gwylim
 */
public class TestRobot {

  private static final String TEST_ROBOT_NAME = "testRobot";


  /**
   * Test {@link Robot}s.
   * Check that they:
   * Subscribe to the {@link MessageSystem} for {@link InstructionBatch}es;
   * Stop when requested.
   */
  @Test
  public void testRobotStartStop() throws Exception {

    final MessageSystem messageSystem = EasyMock.createMock(MessageSystem.class);

    // Expect a subscription for InstructionBatches.
    final Capture<PullSubscription<InstructionBatch>> batchSubscriptionCapture = EasyMock.newCapture();
    messageSystem.subscribe(EasyMock.and(EasyMock.capture(batchSubscriptionCapture), EasyMock.anyObject(PullSubscription.class)));
    EasyMock.expectLastCall();

    // Expect a client to be created for the TubeMap.
    messageSystem.createSynchronousClient(TubeMap.SERVER_NAME, TubeStation.class);
    EasyMock.expectLastCall().andReturn(null); // Not called in this test.

    // Expect the robot to remove it's subscription for instructions as it shuts down.
    messageSystem.unsubscribe(EasyMock.anyObject(PullSubscription.class));

    EasyMock.replay(messageSystem);

    // Create and start robot.
    final Robot robot = new Robot(TEST_ROBOT_NAME, messageSystem);
    robot.start();

    // Get the instruction subscription and send a Stop instruction.
    final PullSubscription<InstructionBatch> robotInstructionBatchSubscription = batchSubscriptionCapture.getValue();
    robotInstructionBatchSubscription.add(new InstructionBatch(Collections.singletonList(new StopInstruction(TEST_ROBOT_NAME, new Date())), TEST_ROBOT_NAME));

    // Wait for the robot to terminate.
    robot.join();
  }


  /**
   * Test {@link Robot}s.
   * Check that they:
   * Subscribe to the {@link MessageSystem} for {@link InstructionBatch}es;
   * requests the location of closest station;
   * file a {@link TrafficReport} when close to a station;
   * Stop when requested.
   */
  @Test
  public void testRobotStationRequest() throws Exception {
    final Location startLocation = new Location(0, 0);
    final Location moveLocation = new Location(1, 1);


    final MessageSystem messageSystem = EasyMock.createMock(MessageSystem.class);

    // Expect a subscription for InstructionBatches.
    final Capture<PullSubscription<InstructionBatch>> batchSubscriptionCapture = EasyMock.newCapture();
    messageSystem.subscribe(EasyMock.and(EasyMock.capture(batchSubscriptionCapture), EasyMock.anyObject(PullSubscription.class)));
    EasyMock.expectLastCall();

    // Expect a client to be created for the TubeMap.
    @SuppressWarnings("unchecked")
    final SynchronousClient<Location, TubeStation> mockTubeMapClient = EasyMock.createMock(SynchronousClient.class);
    messageSystem.createSynchronousClient(TubeMap.SERVER_NAME, TubeStation.class);
    EasyMock.expectLastCall().andReturn(mockTubeMapClient);
    // Expect a call to the TubeMap with the location that the robot moved to.
    mockTubeMapClient.request(moveLocation);
    EasyMock.expectLastCall().andReturn(new TubeStation("TestStation", moveLocation));

    // Expect the publication of a traffic report.
    messageSystem.publish(EasyMock.anyObject(TrafficReport.class));

    // Expect the robot to unsubscribe from receiving InstructionBatches.
    messageSystem.unsubscribe(EasyMock.anyObject(PullSubscription.class));

    EasyMock.replay(messageSystem);
    EasyMock.replay(mockTubeMapClient);


    // Create and start robot.
    final Robot robot = new Robot(TEST_ROBOT_NAME, messageSystem);
    robot.start();

    // Get the instruction subscription and send some move instructions.
    final PullSubscription<InstructionBatch> robotInstructionBatchSubscription = batchSubscriptionCapture.getValue();
    robotInstructionBatchSubscription.add(new InstructionBatch(Arrays.asList(
        new MoveInstruction(TEST_ROBOT_NAME, startLocation, new Date()),
        new MoveInstruction(TEST_ROBOT_NAME, moveLocation, new Date())), TEST_ROBOT_NAME));

    // Stop the robot.
    robotInstructionBatchSubscription.add(new InstructionBatch(Collections.singletonList(new StopInstruction(TEST_ROBOT_NAME, new Date())), TEST_ROBOT_NAME));

    // Wait for the robot to terminate.
    robot.join();
  }
}
