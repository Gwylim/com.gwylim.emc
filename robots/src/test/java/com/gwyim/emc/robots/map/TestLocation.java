package com.gwyim.emc.robots.map;

import org.junit.Assert;
import org.junit.Test;

public class TestLocation {

  @Test
  public void testDistance() {
    final Location base = new Location(51.503071, -0.280303);
    final Location other = new Location(51.504342, -0.275627);

    Assert.assertEquals(353.16, base.distance(other), 0.01);
  }

  @Test
  public void testDistanceSame() {
    final Location base = new Location(51.503071, -0.280303);
    Assert.assertEquals(0, base.distance(base), 0.01);
  }

  /** Regression test for devision by zero. */
  @Test
  public void testDistanceZeroes() {
    final Location base = new Location(0, 0);
    Assert.assertEquals(0, base.distance(base), 0.01);
  }

}
